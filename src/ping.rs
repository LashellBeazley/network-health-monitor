// Copyright (c) 2017 Yarin Licht
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub enum PingResult {
	Reply(::std::time::Duration),
	NoReply,
	Err(String),
}

pub fn ping(peer: ::std::net::IpAddr, timeout: ::std::time::Duration) -> PingResult
{
	let args = ["-q", "-n", "-c", "1", "-W", &(if timeout.as_secs() >= 1 { timeout.as_secs() } else { 1 }).to_string(), &peer.to_string()];
	let output = match ::std::process::Command::new("ping").args(&args).output() {
		Ok(output) => { output }
		Err(err) => { return PingResult::Err("Error executing ping program: ".to_owned() + &err.to_string()) }
	};

	let str = String::from_utf8_lossy(&output.stderr);
	if !str.is_empty() {
		let line = str.lines().next().unwrap_or("");
		let cut_pt = match line.rfind(':') { Some(i) => i + 1, None => 0 };
		let msg = line[cut_pt ..].trim();
		return PingResult::Err(if !msg.is_empty() { msg.to_owned() } else {
			"Unknown error (exit status ".to_owned() + &output.status.to_string() + ")"
		})
	};

	let str = String::from_utf8_lossy(&output.stdout);
	let word = str.lines().nth(4).unwrap_or("").split('/').nth(4).unwrap_or("");
	if word.is_empty() { return PingResult::NoReply };
	let ms = match word.parse::<f64>() {
		Ok(ms) => { ms }
		Err(_) => { return PingResult::Err("Couldn't parse ping return time".to_owned()) }
	};
	let ns = (ms * 1_000_000.0).round() as u64;
	PingResult::Reply(::std::time::Duration::new(ns / 1_000_000_000, (ns % 1_000_000_000) as u32))
}

