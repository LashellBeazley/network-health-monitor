// Copyright (c) 2017 Yarin Licht
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

mod configuration;
mod ping;

fn duration_as_f64(dur: ::std::time::Duration) -> f64
{
	(((dur.as_secs() as u64) * 1_000_000_000) + (dur.subsec_nanos() as u64)) as f64 / 1_000_000_000.0
}

struct PingRecord {
	time:   ::std::time::SystemTime,
	result: ping::PingResult,
}

fn main()
{
	let cf = match configuration::parse_configuration(&::std::env::args().collect::<Vec<String>>()[1..]) {
		Ok(cf) => { cf }
		Err(err) => {
			use std::io::Write;
			writeln!(&mut std::io::stderr(), "{}", err).unwrap_or(());
			return ::std::process::exit(1);
		}
	};

	let log = ::std::sync::Arc::new(::std::sync::Mutex::new(::std::collections::VecDeque::<PingRecord>::new()));

	{ let log = log.clone();
		let peer      = cf.peer.clone();
		let frequency = cf.frequency.clone();
		let timeout   = cf.timeout.clone();
		::std::thread::spawn(move|| {
			loop {
				let log = log.clone();
				::std::thread::spawn(move|| {
					let record = PingRecord{result: ping::ping(peer, timeout), time: ::std::time::SystemTime::now()};
					log.lock().unwrap().push_back(record);
				});
				::std::thread::sleep(frequency);
			}
		});
	};

	if !cf.raw_output {
		use std::io::Write;
		write!(&mut std::io::stdout(),
		       "[ Reliability:   0% | Average Latency:    0ms | Latency Variance:    0ms | Local Status: Waiting ]").unwrap_or(());
		::std::io::stdout().flush().unwrap_or(());

		while log.lock().unwrap().is_empty() { ::std::thread::sleep(cf.frequency); };
	};

	loop {
		let mut latency_accum      = ::std::time::Duration::new(0, 0);
		let mut latency_max        = ::std::time::Duration::new(0, 0);
		let mut latency_min        = cf.timeout;
		let mut n_successful_pings = 0 as usize;
		let mut n_failed_pings     = 0 as usize;
		let mut recent_err         = "".to_owned();

		{ let mut log = log.lock().unwrap();

			let age_limit = ::std::time::SystemTime::now() - ::std::time::Duration::new(10, 0);
			while !log.is_empty() && log[0].time < age_limit { let _ = log.pop_front(); }

			for record in log.iter() {
				match record.result {
					ping::PingResult::Reply(time) => { n_successful_pings += 1;
						latency_accum += time;
						if time > latency_max { latency_max = time; }
						if time < latency_min { latency_min = time; }
					}
					ping::PingResult::NoReply     => { n_failed_pings += 1; }
					ping::PingResult::Err(_)      => { n_failed_pings += 1; }
				}
			};

			if !log.is_empty() {
				match &log[log.len() - 1].result {
					&ping::PingResult::Err(ref err) => { recent_err = err.to_owned(); }
					_ => { }
				};
			};
		};

		let mut reliability      = 0.0;
		let mut average_latency  = 0.0; //::std::f64::NAN;
		let mut latency_variance = 0.0; //::std::f64::NAN;
		if n_successful_pings > 0 {
			let avg = latency_accum / (n_successful_pings as u32);
			reliability      = (n_successful_pings as f64) / ((n_successful_pings + n_failed_pings) as f64);
			average_latency  = duration_as_f64(avg);
			latency_variance = duration_as_f64(::std::cmp::max(latency_max - avg, avg - latency_min));
		};

		if cf.raw_output {
			println!("{:.0} {:.0} {:.0} {}", reliability * 100.0, average_latency * 1_000.0, latency_variance * 1_000.0, recent_err);

		} else {
			let lat_var_ratio = latency_variance / (average_latency + 0.1); // 10ms
			let rel_color     = if reliability     >= 0.99 { "92" } else if reliability     >= 0.95 { "93" } else { "91" };
			let avg_lat_color = if average_latency <= 0.1  { "92" } else if average_latency <= 0.2  { "93" } else { "91" };
			let lat_var_color = if lat_var_ratio   <= 0.1  { "92" } else if lat_var_ratio   <= 0.3  { "93" } else { "91" };
			let status_color  = if recent_err.is_empty() { "92" } else { "91" };
			let status        = if recent_err.is_empty() { "OK" } else { &recent_err };

			use std::io::Write;
			write!(&mut std::io::stdout(), concat!("\x1b[G",
			       "[ Reliability: \x1b[{}m{:3.0}%\x1b[m | Average Latency:\x1b[{}m{:5.0}ms\x1b[m | ",
			       "Latency Variance:\x1b[{}m{:5.0}ms\x1b[m | Local Status: \x1b[{}m{}\x1b[m ]", "\x1b[K"),
				   rel_color, reliability * 100.0, avg_lat_color, average_latency * 1_000.0,
				   lat_var_color, latency_variance * 1_000.0, status_color, status).unwrap_or(());
			::std::io::stdout().flush().unwrap_or(());
		};

		::std::thread::sleep(cf.frequency);
	};
}

