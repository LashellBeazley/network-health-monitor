// Copyright (c) 2017 Yarin Licht
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub struct Configuration {
	pub peer:       ::std::net::IpAddr,
	pub frequency:  ::std::time::Duration,
	pub timeout:    ::std::time::Duration,
	pub window:     ::std::time::Duration,
	pub raw_output: bool,
}

const DEFAULT_PEER:      &'static [u8] = &[8, 8, 8, 8]; // &[198, 41, 0, 4];
const DEFAULT_FREQUENCY: u64           =    500_000_000;
const DEFAULT_TIMEOUT:   u64           = 10_000_000_000;
const DEFAULT_WINDOW:    u64           =  5_000_000_000;

pub fn parse_configuration(args: &[String]) -> Result<Configuration, String>
{
	extern crate getopts;
	let mut opts = getopts::Options::new();
	opts.optopt("p", "peer", "IP address of peer to ping", "ADDRESS");
	opts.optopt("f", "frequency", "Frequency at which to send pings", "SECONDS");
	opts.optopt("t", "timeout", "Maximum time to wait for replies to pings", "SECONDS");
	opts.optopt("w", "window", "Lifetime of ping receipts used for sampling", "SECONDS");
	opts.optflag("h", "help", "Show this message");

	let matches = match opts.parse(args) {
		Ok(matches) => { matches }
		Err(err) => { return Err("Unable to parse arguments".to_owned() + ": " + &err.to_string()); }
	};

	if matches.opt_present("h") {
		let brief = format!("Usage: network-health-monitor [options]");
		return Err(opts.usage(&brief));
	};

	let peer = match matches.opt_str("p") {
		Some(str) => {
			match str.parse::<::std::net::IpAddr>() {
				Ok(addr) => { addr }
				Err(err) => { return Err("Unable to parse peer address".to_owned() + ": " + &err.to_string() + ": " + &str); }
			}
		}
		None => { ::std::net::IpAddr::V4(::std::net::Ipv4Addr::new(DEFAULT_PEER[0], DEFAULT_PEER[1], DEFAULT_PEER[2], DEFAULT_PEER[3])) }
	};
	
	fn parse_duration(matches: &getopts::Matches, name: &str, code: &str, default: u64) -> Result<::std::time::Duration, String>
	{
		let ns = match matches.opt_str(code) {
			Some(str) => {
				match str.parse::<f64>() {
					Ok(n)    => { (n * 1_000_000_000.0).round() as u64 }
					Err(err) => { return Err("Unable to parse ".to_owned() + name + " number: " + &err.to_string() + ": " + &str) }
				}
			}
			None => { default }
		};
		Ok(::std::time::Duration::new(ns / 1_000_000_000, (ns % 1_000_000_000) as u32))
	}

	let frequency = match parse_duration(&matches, "frequency", "f", DEFAULT_FREQUENCY) { Ok(n) => { n } Err(err) => { return Err(err) } };
	let timeout   = match parse_duration(&matches, "timeout",   "t", DEFAULT_TIMEOUT)   { Ok(n) => { n } Err(err) => { return Err(err) } };
	let window    = match parse_duration(&matches, "window",    "w", DEFAULT_WINDOW)    { Ok(n) => { n } Err(err) => { return Err(err) } };

	if !matches.free.is_empty() {
		return Err("Invalid option: ".to_owned() + &matches.free[0]);
	}

	extern crate isatty;
	Ok(Configuration{peer: peer, frequency: frequency, timeout: timeout, window: window, raw_output: !isatty::stdout_isatty()})
}

