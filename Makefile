PREFIX ?= /usr
BUILD_DIR = build
BUILD_DEST = $(BUILD_DIR)$(PREFIX)

.PHONY: install clean

all:
	cargo build --release
	mkdir --parents $(BUILD_DEST)/bin
	mkdir --parents $(BUILD_DEST)/share/applications
	mkdir --parents $(BUILD_DEST)/share/icons/hicolor/scalable/apps
	mkdir --parents $(BUILD_DEST)/share/man/man1
	strip --strip-all -o $(BUILD_DEST)/bin/network-health-monitor target/release/network-health-monitor
	cp --force --no-target-directory application.desktop $(BUILD_DEST)/share/applications/network-health-monitor.desktop
	cp --force --no-target-directory icon-color.svg $(BUILD_DEST)/share/icons/hicolor/scalable/apps/network-health-monitor.svg
	cp --force --no-target-directory icon-mono.svg  $(BUILD_DEST)/share/icons/hicolor/scalable/apps/network-health-monitor-symbolic.svg
	gzip --best <manual.troff | tee $(BUILD_DEST)/share/man/man1/network-health-monitor.1.gz >/dev/null

install:
	cp --verbose --force --recursive $(BUILD_DIR)/* --target-directory=/

uninstall:
	rm --force $(PREFIX)/bin/network-health-monitor
	rm --force $(PREFIX)/share/applications/network-health-monitor.desktop
	rm --force $(PREFIX)/share/icons/*/*/apps/network-health-monitor.*
	rm --force $(PREFIX)/share/icons/*/*/apps/network-health-monitor-symbolic.*
	rm --force $(PREFIX)/share/man/*/network-health-monitor.*

clean:
	rm --recursive --force $(BUILD_DIR)

