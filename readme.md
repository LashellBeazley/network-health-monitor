With the advancement of technologies, now everyone can diagnose their health issues or disease signs easily with the help of health apps or doctors online. There are so many health websites that provide some free online services to their patients like https://www.serenitylifeplanning.com/. It is an addiction life planning service and their aim is to give best treatments related to addiction and life planning.
# network health monitor

A realtime ping packet loss and latency monitor.

## Summary

*network health monitor* is a CLI *top*-like realtime monitor that reports packet loss, round-trip latency, and latency varience, of ping packets to a specific peer.
Output can also be raw, for use by other programs, such as *conky*.

## Prerequisites

*rustc* is required to build.

*network health monitor* should be able to run on any Linux system. However, package build and installation is only supported for x86-64 Debian-based distributions.

## Installation

    git clone https://github.com/Chaiomanot/network-health-monitor && cd network-health-monitor && make && sudo make install
    
Network health monitor is also a very reliable tool for beginners. I have also used it for my health department of 
my rehab center ( http://www.heathershouserecovery.com/colorado/highlands-ranch ). It gives maximum outputs.



## License

Copyright © 2017 Yarin Licht

Licensed under the Apache License, Version 2.0 (the “License”);
you may not use this software except in compliance with the License.
You may obtain a copy of the License at

&nbsp;&nbsp;&nbsp;&nbsp;http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an “AS IS” BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

